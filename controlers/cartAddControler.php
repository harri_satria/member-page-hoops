<?php
require_once "../config.php";

session_start();


$cartType = $_POST["cartType"];
$memberId = $_POST["idMember"];

function genCartId($c){
    $sql = "SELECT id_cart FROM hoops_point_cart ORDER BY id_cart DESC";
    $res = $c->prepare($sql);
    $res->execute();
    $data_id = $res->fetchColumn();
   
    if ($data_id !== null){
        $oid = intVal($data_id) + 1;
        return $oid;
    }elseif ($data_id == null){
        return 1;
    }
}

function addCart($c,$mId,$ct){
    $sql = "INSERT INTO hoops_point_cart (id_cart,id_member,cart_type,date_add)
        VALUES (:idCart,:id_member,:cart_type,NOW())
    ";

    $res = $c->prepare($sql);
    $res->execute([
        "idCart" => genCartId($c),
        "id_member" => $mId,
        "cart_type" => $ct
    ]);

    if ($res){
        $sqlId = "SELECT id_cart FROM hoops_point_cart WHERE id_member = ? AND id_order = 0";
        $state = $c->prepare($sqlId);
        $state->execute([$mId]);
        $stateData = $state->fetchColumn();
        $_SESSION["idCart"] = $stateData;
        //echo "idCart = ". $stateData;
    }else{
        echo 500;
    }
}

$cekCart = "SELECT id_cart FROM hoops_point_cart WHERE id_member = ? AND id_order = 0";
$cek = $conn->prepare($cekCart);
$cek->execute([$memberId]);
$cekResult = $cek->fetchColumn();
$cartCount = $cek->rowCount();
if ($cartCount == 1){
    $_SESSION["idCart"] = $cekResult;
    echo "idCart -> ".$cekResult;
}else if ($cartCount > 1){
    $removeCart = "DELETE FROM hoops_point_cart WHERE id_member = ? AND id_order = 0"; // ERROR : HAPUSNYA CUMA 1 
    $stateRemove = $conn->prepare($removeCart);
    $stateRemove->execute([$memberId]);
    if ($stateRemove){
        addCart($conn,$memberId,$cartType);
    }
}else if ($cartCount == 0){
    addCart($conn,$memberId,$cartType);
}

$conn = null;
?>