<?php
require_once "../config.php";

session_start();

$memberId = $_SESSION["memberId"];


if (isset($_GET["from"]) && $_GET["from"] == "3"){
   $sql = "SELECT
      hoops_point_cart.id_cart,
      hoops_point_cart.id_order,
      hoops_point_cart.id_member,
      hoops_point_cart.`date_add`,
      hoops_point_cart.id_carrier,
      hoops_point_cart.cart_type,
      hoops_point_cart.id_address,

      hoops_point_item.id_cart,
      hoops_point_item.id_item,
      hoops_point_item.item_name,
      hoops_point_item.poin,
      hoops_point_item.exp_item
      FROM
      hoops_point_cart
      Inner Join hoops_point_item ON hoops_point_cart.id_cart = hoops_point_item.id_cart
      WHERE hoops_point_cart.id_member = ? AND hoops_point_cart.id_order = 0
   ";
   $count_cart = $conn->prepare($sql);
   $count_cart->execute([$memberId]);
   $data_count = $count_cart->rowCount();

   echo json_encode($data_count);
}

if (isset($_GET["from"]) && $_GET["from"] == "2"){
   $dataArr = array();

   $sql = "SELECT
      hoops_point_cart.id_cart,
      hoops_point_cart.id_order,
      hoops_point_cart.id_member,
      hoops_point_cart.`date_add`,
      hoops_point_cart.id_carrier,
      hoops_point_cart.cart_type,
      hoops_point_cart.id_address,

      hoops_point_item.id_cart,
      hoops_point_item.id_item,
      hoops_point_item.item_name,
      hoops_point_item.poin,
      hoops_point_item.exp_item,
      hoops_point_item.item_type
      FROM
      hoops_point_cart
      Inner Join hoops_point_item ON hoops_point_cart.id_cart = hoops_point_item.id_cart
      WHERE hoops_point_cart.id_member = ? AND hoops_point_cart.id_order = 0
   ";
   $res = $conn->prepare($sql);
   $res->execute([$memberId]);
   $data = $res->fetchAll();

   foreach ($data as $row){   
      $dataArr[] =array(
         "id_cart" => $row["id_cart"],
         "id_address" => $row["id_address"],
         "id_carrier" => $row["id_carrier"],
         "id_member" => $row["id_member"],
         "cart_type" => $row["cart_type"],
         "item" => $row["item_name"],
         "poin" => $row["poin"],
         "date_add" => $row["date_add"],
         "item_type" => $row["item_type"],
         "total" => $res->rowCount()
      ); 
   }
   echo json_encode($dataArr);
}

if (isset($_GET["from"]) && $_GET["from"] == "1"){
   $dataArr["data"] = array();

   $sql = "SELECT
      hoops_point_cart.id_cart,
      hoops_point_cart.id_order,
      hoops_point_cart.id_member,
      hoops_point_cart.`date_add`,
      hoops_point_cart.id_carrier,
      hoops_point_cart.cart_type,
      hoops_point_cart.id_address,

      hoops_point_item.id,
      hoops_point_item.id_cart,
      hoops_point_item.id_item,
      hoops_point_item.item_name,
      hoops_point_item.poin,
      hoops_point_item.exp_item,
      hoops_point_item.item_type,
      hoops_point_item.size,
      hoops_point_item.description,
      hoops_point_item.id_product
      FROM
      hoops_point_cart
      Inner Join hoops_point_item ON hoops_point_cart.id_cart = hoops_point_item.id_cart
      WHERE hoops_point_cart.id_member = ? AND hoops_point_cart.id_order = 0
   ";


   $res = $conn->prepare($sql);
   $res->execute([$memberId]);
   $data = $res->fetchAll();


   foreach ($data as $row){   
      $dataArr["data"][] = $row; 
   }
   
   echo json_encode($dataArr);
}

$conn = null;
?>