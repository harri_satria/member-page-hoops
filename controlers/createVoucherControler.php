<?php 
require_once "../config.php";

session_start();

date_default_timezone_set('Asia/Jakarta');
$dt = date("Y-m-d h:m:s");

$data = $_POST["dataArray"]; 
$memberId = $_SESSION["memberId"];
$userId = $_SESSION["userId"];

function addHistories($c,$d,$cid){
    $ins = "INSERT INTO hoops_point_histories (
        member_id,
        customer_id,
        transaction_number,
        transaction_date,
        transaction_value,
        point_value,
        outlet_id,
        `type`,
        source_transactions,
        createdAt
    )VALUES(
        :memberId,
        :customerId,
        :txnNumber,
        NOW(),
        :txnValue,
        :pointValue,
        :outletId,
        :type,
        :source,
        NOW()
    )";
    for ($i=0; $i < count($d); $i++){
        $stateIns = $c->prepare($ins);
        $stateIns->execute([
            "memberId" => $d[$i][2],
            "customerId" => $cid,
            "txnNumber" => $d[$i][10],   
            "txnValue" => 0,
            "pointValue" => $d[$i][11],
            "outletId" => 0,
            "type" => "decrease",
            "source" => "SYSTEM"
        ]);
    }
}

function getSumCart($c,$idCart){
    $sql = "SELECT
    SUM(hoops_point_item.poin) AS total
    FROM
    hoops_point_cart
    Inner Join hoops_point_item ON hoops_point_cart.id_cart = hoops_point_item.id_cart
    WHERE hoops_point_cart.id_cart = ?
    ";

    $res = $c->prepare($sql);
    $res->execute([$idCart]);
    $data = $res->fetchColumn();

    return $data;
}

function updatePoin($c,$id,$used){
    $sqlState = "UPDATE hoops_members SET pointstate = pointstate - ? WHERE id = ?";

    $stateDecrease = $c->prepare($sqlState);
    $stateDecrease->execute([$used,$id]);

    if ($stateDecrease){
        $sqlUsed = "UPDATE hoops_members SET pointused = pointused + ? WHERE id = ?";
        
        $stateUsed = $c->prepare($sqlUsed);
        $stateUsed->execute([$used,$id]);
    }else{
        echo "500";
    }
}

function generateRandomString($length) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function genCartId($c){
    $sql = "SELECT id_cart FROM hoops_point_cart ORDER BY id_cart DESC";
    $res = $c->prepare($sql);
    $res->execute();
    $data_id = $res->fetchColumn();
   
    if ($data_id !== null){
        $oid = intVal($data_id) + 1;
        return $oid;
    }elseif ($data_id == null){
        return 1;
    }
}

function genOrderId($c){
    $sql = "SELECT id FROM hoops_point_redeem ORDER BY id DESC";
    $res = $c->prepare($sql);
    $res->execute();
    $data_id = $res->fetchColumn();
   
    if ($data_id !== null){
        $oid = intVal($data_id) + 1;
        return $oid;
    }elseif ($data_id == null){
        return 1;
    }
}

function getlatestIdCart($c,$m){
    $sql = "SELECT id_cart FROM hoops_point_cart WHERE id_member=? AND id_order = 0";
    $res = $c->prepare($sql);
    $res->execute([$m]);
    $data = $res->fetchColumn();

    return $data;
}

function addCart($c,$mId,$ct){
    $sql = "INSERT INTO hoops_point_cart (id_cart,id_member,cart_type,date_add)
        VALUES (:idCart,:id_member,:cart_type,NOW())
    ";
    
    $res = $c->prepare($sql);
    $res->execute([
        "idCart" => genCartId($c),
        "id_member" => $mId,
        "cart_type" => $ct
    ]);
  
}

function updateTotalPoinCart($c,$sc,$idCartUp,$m){
    $sqlUpdate = "UPDATE hoops_point_cart SET total_poin = ? WHERE id_cart = ? AND id_member = ?";
    $state = $c->prepare($sqlUpdate);
    $state->execute([$sc,$idCartUp,$m]);
}

// PINDAH KE ATAS SEBELUM ORDER DI TAMBAH, KALO DI BAWAH JADI NAMBAH 1 ORDER ID NYA

for ($i=0; $i < count($data); $i++){
    if (isset($data) && $data[$i][13] == 1){  print_r ($data);
        $sql = "INSERT INTO hoops_voucher_lists 
        (userId,voucher_id,voucher_code,voucher_exp,`from`,createdAt)
        VALUES (:userId,:voucherId,:voucherCode,:voucherExp,:getfrom,NOW())";

        echo "voucher";
        $state = $conn->prepare($sql);
        $state->execute([
            "userId" => $_SESSION["userId"],
            "voucherId" => $data[$i][9],
            "voucherCode" =>generateRandomString(7),
            "voucherExp" => $data[$i][12],
            "getfrom"  => "GIVE",
        ]);
    }else if (isset($data) && $data[$i][13] == 2){
        echo " coupon ";
        $sql = "INSERT INTO hoops_raffles_winners (raffleId,customerId,productId,size_id,createdAt,updatedAt,shipping_type,kloter)
        VALUES (:raffleId,:customerId,:productId,:sizeId,NOW(),NOW(),:shipping,:kloter)";

        $state = $conn->prepare($sql);
        $state->execute([
            "raffleId" => $data[$i][9],
            "customerId" => $userId,
            "productId" => $data[$i][16],
            "sizeId" => $data[$i][14],
            "shipping" => $data[$i][15],
            "kloter" => 1
        ]);
    }
} 

if ($state){    
    $sql_update = "UPDATE hoops_point_cart SET id_order = :idOrder WHERE id_cart = :idCart "; 
    $res_u = $conn->prepare($sql_update);
    for ($x=0; $x < count($data); $x++){
        $res_u->execute([
            "idOrder" => genOrderId($conn),
            "idCart" => $data[$x][0]
        ]);
    }

    if ($res_u){
        $ins_order = "INSERT INTO hoops_point_redeem (id,id_member,date_create) VALUES (:id,:idMember,NOW())";
        $state_order = $conn->prepare($ins_order);
        $latest_oid = genOrderId($conn); 
        $state_order->execute([
            "id" => $latest_oid,
            "idMember" => $memberId,
        ]);
        updatePoin($conn,$memberId,getSumCart($conn,$_SESSION["idCart"]));
        updateTotalPoinCart($conn,getSumCart($conn,$_SESSION["idCart"]),$_SESSION["idCart"],$memberId);
        addCart($conn,$memberId,"V"); 
        addHistories($conn,$data,$userId);
        $_SESSION["idCart"] = getlatestIdCart($conn,$memberId);

    }else{
        echo "Id Order cannot generated..";
    }
}else{
    echo "Cannot create voucher..";
}

$conn=null;
?>