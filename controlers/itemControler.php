<?php
require_once "../config.php";

session_start();

$userId = $_SESSION["userId"];
$memberId = $_SESSION["memberId"];
$idCart = $_SESSION["idCart"];
$cart_type = $_POST["cart_type"];
$item_name = $_POST["item_name"];
$poin = $_POST["poin"];
$idItem = $_POST["idItem"];
$expItem = $_POST["expItem"];
$itemType = $_POST["itemType"];
$size = $_POST["size"];
$desc = $_POST["desc"];
$idProductRaffle = $_POST["idProductRaffle"];


/*$ins_cart = "INSERT INTO hoops_point_cart (id_cart,id_member,cart_type,date_add)
    VALUES (:idCart,:id_member,:cart_type,:date_add)
";*/

$ins_item = "INSERT INTO hoops_point_item (id_item,id_cart,item_name,poin,exp_item,item_type,size,`description`,id_product) 
    VALUES (:idItem,:idCart,:itemName,:poin,:expItem,:itemType,:size,:descr,:idProduct)
";

$state_item = $conn->prepare($ins_item);
$state_item = $state_item->execute([
    "idItem" => $idItem,
    "idCart" => $idCart,
    "idProduct" => $idProductRaffle,
    "itemName" => $item_name,
    "poin" => $poin,
    "expItem" => $expItem,
    "itemType" => $itemType,
    "size" => $size,
    "descr" => $desc
]);
/*
$state = $conn->prepare($ins_cart);
$state->execute([
    "idCart" => genCartId($conn),
    "id_member" => $_SESSION["memberId"],
    "cart_type" => $cart_type,
    "date_add" => date("Y-m-d h:m:s")
]); */

$conn=null;
?>