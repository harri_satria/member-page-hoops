<?php 
require_once "../config.php";
session_start();
$paramArr=array();
$email = strtolower($_POST["user"]);
$pwd = hash(hash_algos()[5], $_POST["password"],false);

$sql = "SELECT * FROM customers WHERE email = ? AND password=?";
$res = $conn->prepare($sql);
$res->execute([$email,$pwd]);

while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
    $paramArr = [
        "id" => $row["id"],
        "email" => strtolower($row["email"]),
        "id_member" => $row["id_member"]
    ];
}

if (!empty($paramArr)){
    $_SESSION["LASTLOGIN"] = time();
    $_SESSION["userId"] = $paramArr["id"];
    $_SESSION["memberId"] = $paramArr["id_member"]; 
    echo json_encode($paramArr);
}
$conn=null;
?>