<?php
require_once "../config.php";

session_start();

$userId = $_SESSION["userId"];
$data["data"]=array();

$select = "SELECT
hoops_voucher_lists.id,
hoops_voucher_lists.reedem,
hoops_master_voucher.card_name,
hoops_master_voucher.card_type,
hoops_master_voucher.card_desc,
hoops_master_voucher.card_value,
hoops_master_voucher.card_minimum_transaction,
hoops_master_voucher.poin_required,
hoops_master_voucher.card_start,
hoops_master_voucher.card_end,
hoops_voucher_lists.createdAt,
hoops_voucher_lists.updatedAt
FROM
hoops_voucher_lists
Inner Join hoops_master_voucher ON hoops_voucher_lists.voucher_id = hoops_master_voucher.id
where userId=:userId and card_desc = :desc ORDER BY createdAt DESC
";

$stateSel = $conn->prepare($select);
$stateSel->execute([
    "userId" => $userId,
    "desc" => "poin"
]);

while ($row = $stateSel->fetch(PDO::FETCH_ASSOC)) {
    $data["data"][] = $row; 
}

echo json_encode($data);

$conn=null;

?>