<?php
require_once "../config.php";

session_start();

$idMember = $_GET["idMember"];

$data = array();

$select = "SELECT * FROM hoops_point_testimonial WHERE id_member=:idMember";

$stateSel = $conn->prepare($select);
$stateSel->execute([
    "idMember" => $idMember
]);
while ($row = $stateSel->fetch(PDO::FETCH_ASSOC)) {
    $data=[
        "opini" => $row["comment"],
        "dateCreate" => $row["date_create"]
    ];
}   
if (!empty($data)){
    echo json_encode($data);
}
$conn=null;
?>