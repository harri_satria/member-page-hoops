<?php
require_once "../config.php";
session_start();

$userId = $_SESSION["userId"];
$dataArr = array();

$sql = "SELECT * FROM hoops_members WHERE idcustomer = ?";
$res = $conn->prepare($sql);
$res->execute([$userId]);
$data = $res->fetchAll();

foreach ($data as $row){
    $dataArr=[
        "idmember" => $row["id"],
        "create_date" => $row["createdAt"],
        "identity_number" => $row["iden_number"]."&nbsp;(".$row["identity"].")",
        "name"  => $row["firstname"]." ".$row["lastname"],
        "phone" => $row["phone"],
        "email" => $row["email"],
        "address" => $row["address"],
        "city" => $row["city"],
        "poin" => $row["pointstate"] 
    ];
}
echo json_encode($dataArr);
$conn = null;
?>