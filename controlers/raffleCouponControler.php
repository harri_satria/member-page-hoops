<?php
require_once "../config.php";

session_start();

$memberId = $_SESSION["memberId"];
$userId = $_SESSION["userId"];
$dataArray= array();

$sql = "SELECT
hoops_raffles.id,
hoops_raffles.raffle_name,
hoops_raffles.rafle_type,
hoops_raffles.start_date,
hoops_raffles.end_date,
hoops_raffles.raffle_point,
hoops_raffles.`status`,
hoops_raffles.createdAt,
hoops_raffles.updatedAt,
hoops_raffles.carousel_id,
hoops_raffles.published,
hoops_raffles.last_url_exp,
hoops_raffle_product_details.limit_user
FROM
hoops_raffle_product_details
Inner Join hoops_raffles ON hoops_raffle_product_details.raffle_id = hoops_raffles.id
WHERE published IS NULL AND end_date >= NOW()
GROUP BY id
";

$stateRaffle = $conn->prepare($sql);
$stateRaffle->execute([]);
$data = $stateRaffle->fetchAll();

foreach ($data as $row){
    $row["poin_required"] = 25000; // DIRUBAH TIAP EVENT BARU MANUAL
    $dataArray[]=$row;
}

echo json_encode($dataArray);

$conn=null;
?>