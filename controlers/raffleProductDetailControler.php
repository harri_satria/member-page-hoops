<?php
require_once "../config.php";

session_start();

$memberId = $_SESSION["memberId"];
$userId = $_SESSION["userId"];
$idRaffle = $_GET["idRaffle"];

$dataArray= array();

$sql = "SELECT
hoops_raffles.id,
hoops_raffle_product_details.raffle_product_id,
hoops_raffle_product_details.size_id,
hoops_master_sizes.size
FROM
hoops_raffles
Inner Join hoops_raffle_product_details ON hoops_raffles.id = hoops_raffle_product_details.raffle_id
Inner Join hoops_master_sizes ON hoops_raffle_product_details.size_id = hoops_master_sizes.id
where hoops_raffles.id = ?
";

$stateRaffleDetail = $conn->prepare($sql);
$stateRaffleDetail->execute([$idRaffle]);
$data = $stateRaffleDetail->fetchAll();

foreach ($data as $row){
    $dataArray[] = $row;
}

echo json_encode($dataArray);

$conn=null;
?>