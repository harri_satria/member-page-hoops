<?php
require_once "../config.php";

session_start();

$userId = $_SESSION["userId"];
$raffleId = $_POST["raffleId"];
$sizeId = $_POST["sizeId"];
$shipping = $_POST["shipping"];
$productId = $_POST["productId"];

$sql = "INSERT INTO hoops_raffles_winners (raffleId,customerId,productId,size_id,createdAt,updatedAt,shipping_type,kloter)
VALUES (:raffleId,:customerId,:productId,:sizeId,NOW(),NOW(),:shipping,:kloter)";

$stateInsertWinner = $conn->prepare($sql);
$stateInsertWinner->execute([
    "raffleId" => $raffleId,
    "customerId" => $userId,
    "productId" => $productId,
    "sizeId" => $sizeId,
    "shipping" => $shipping,
    "kloter" => 1
]);

if ($stateInsertWinner){
    echo 200;
}else{
    echo 500;
}
?>