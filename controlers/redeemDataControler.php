<?php
require_once "../config.php";
session_start();

$memberId = $_SESSION["memberId"];
$dataArr["data"] = array();

$sql = "
SELECT
hoops_point_redeem.id_cart,
hoops_point_redeem.id_member,
hoops_point_redeem.id_carrier,
hoops_point_redeem.date_create,
hoops_point_redeem.id,
hoops_point_cart.total_poin,
hoops_point_cart.date_add,
hoops_point_cart.id_cart,
hoops_point_cart.id_order
FROM
hoops_point_redeem
Inner Join hoops_point_cart ON hoops_point_redeem.id = hoops_point_cart.id_order
WHERE hoops_point_redeem.id_member = ?
";
$res = $conn->prepare($sql);
$res->execute([$memberId]);
$data = $res->fetchAll();

foreach ($data as $row){
    $dataArr["data"][] = $row;
}

echo json_encode($dataArr);

$conn = null;
?>