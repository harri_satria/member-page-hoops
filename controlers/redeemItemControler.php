<?php
require_once "../config.php";

session_start();

$memberId = $_SESSION["memberId"];
$idCartSelected = $_POST["idCartSelected"];
$arrayData = array();

$sql = "SELECT * FROM hoops_point_item WHERE id_cart = ?";

$res = $conn->prepare($sql);
$res->execute([$idCartSelected]);
$data = $res->fetchAll();

foreach ($data as $row){
    $arrayData["data"][] = $row; 
}

echo json_encode($arrayData);

$conn = null;
?>