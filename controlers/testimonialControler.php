<?php
require_once "../config.php";

session_start();

$data["data"]=array();

$select = "SELECT
hoops_point_testimonial.id,
hoops_point_testimonial.id_member,
hoops_point_testimonial.`comment`,
hoops_point_testimonial.date_create,
hoops_members.firstname,
hoops_members.lastname
FROM
hoops_point_testimonial
Inner Join hoops_members ON hoops_members.id = hoops_point_testimonial.id_member
ORDER BY date_create DESC";

$stmt = $conn->prepare($select);
$stmt->execute([]);
$result = $stmt->fetchAll();

foreach ($result as $row){
    $data["data"][] = $row;
}

echo json_encode($data);

$conn=null;
?>