<?php
require_once "../config.php";

session_start();

$memberId = $_SESSION["memberId"];
$comment = $_POST["comment"];

$cek = "SELECT COUNT(*) FROM hoops_point_testimonial WHERE id_member = :idmember";

$stateCek = $conn->prepare($cek);
$stateCek->execute([
    "idmember" => $memberId
]);
$count = $stateCek->fetchColumn();

if ($count == 0){
    $sql = "INSERT IGNORE INTO hoops_point_testimonial (id_member,comment,date_create) VALUES
    (:idMember,:comment,NOW())
    ";

    $stateInsert = $conn->prepare($sql);
    $stateInsert->execute([
        "idMember" => $memberId,
        "comment" => $comment
    ]);

    if ($stateInsert){
        echo 200;
    }else{
        echo 500;
    }
}else if ($count >= 1){
    echo 500;
}
$conn=null;
?>