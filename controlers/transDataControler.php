<?php
require_once "../config.php";
session_start();

$userId = $_SESSION["userId"];
$dataArr = array();

$sql = "SELECT * FROM hoops_point_histories WHERE customer_id = ? AND type = 'increase' ORDER BY transaction_date DESC";
$res = $conn->prepare($sql);
$res->execute([$userId]);
$data = $res->fetchAll();

foreach ($data as $row){
    $dataArr["data"][] = $row;
}
echo json_encode($dataArr);
?>