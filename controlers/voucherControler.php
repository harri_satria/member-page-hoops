<?php
require_once "../config.php";
session_start();

$userId = $_SESSION["userId"];
$dataArr = array();

$sql = "SELECT * FROM hoops_master_voucher WHERE card_desc =? AND card_end >= NOW() AND card_active = 1";
$res = $conn->prepare($sql);
$res->execute(["poin"]);
$data = $res->fetchAll();

foreach ($data as $row){
    $dataArr[] = array(
        "id" => $row["id"],
        "card_name" => $row["card_name"],
        "card_value" => $row["card_value"],
        "card_start" => $row["card_start"],
        "card_end" => $row["card_end"],
        "poin_required" => $row["poin_required"],
        "min_trans" => $row["card_minimum_transaction"],
        "img" => $row["img"]
    );
    
}
echo json_encode($dataArr);

$conn = null;
?>