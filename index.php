<?php
    session_start();
    if (isset($_SESSION["LASTLOGIN"]) || isset($_SESSION["userId"]) || isset($_SESSION["memberId"])) {
        header("location:page.php");
    }else{
    
?>
<html>
    <head lang="en">
        <title></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title></title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-light bg-dark">
            <a class="navbar-brand" href="#" style="color: whitesmoke;">
              <img src="assets/img/hoops_white.png" width="40" height="30" alt="">&nbsp; Membership
            </a>
        </nav>
        <div class="container">
            <div class="row clearfix" style="padding-bottom: 50px;">
                <div class="col-md-8">
                    <div class="col-md-12">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="assets/img/slider/slide0.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/slider/slide1.jpg" alt="Second slide">
                            </div>
                            <!--
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/slider/slide3.png" alt="Third slide">
                            </div> -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12" >
                    <!--  <div class="text-center" style="font-size:1.5rem;padding-bottom:20px;">
                            <span>Hoops Indonesia <strong>Membership</strong></span>
                        </div> -->
                        <div class="card border-info">
                            <div class="card-header"><strong>Login using membership account</strong></div>
                            <div class="card-body">
                                <form id="login">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="email" class="form-control" name="user" id="user" required>
                                        <small id="help user" class="form-text text-muted">Enter the email that corresponds to the web account</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" required>
                                        <small id="help password" class="form-text text-muted">Enter the password that corresponds to the web account</small>
                                    </div>
                                    
                                    <button type="submit" id="btn-submit" class="btn btn-dark btn-block">Login</button>
                                    <a href="https://www.hoopsindonesia.co.id/content/22-member-hoops-indonesia/" class="btn btn-dark btn-block">Register</a>
                                    
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 1rem;">
                        <div class="card border-info" style="max-height: 150px;">
                            <div class="card-header">Testimonials</div>
                            <div class="card-body" style="overflow-y:scroll;padding: 0.5rem;" id="testi-area">
                                
                            </div>
                        </div>
                    </div>
                </div> 
            </div>

            <hr>
            <div class="col-md-4" style="padding-top: 0.7rem; padding-bottom: 2rem;color: grey;">
                &copy; Hoops Indonesia 2022. 
            </div>
    
        </div>





        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/notify.js"></script>
        <script src="js/login.js"></script>
        
    </body>
    
</html>
<?php } ?>