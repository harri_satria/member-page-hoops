
            $(function(){
                function addCartVirtual(idMember){
                    $.ajax({
                        url:"controlers/cartAddControler.php",
                        type:"POST",
                        data:{"cartType":"V","idMember":idMember},
                        success:function(res){
                            console.log(res)
                        }
                    });
                }

                function submitDataLogin(){
                    $.ajax({
                        url:"controlers/loginControler.php",
                        type:"POST",
                        data:$("#login").serialize(),
                        success:function(res){ 
                            if (res !== ""){
                                let data = JSON.parse(res); //console.log(data);
                                console.log(data.email);
                                if (data.id_member == "" || data.id_member == null){
                                    console.log("no member found");
                                    $.notify("Oops..you are not registered as a member...!", "danger");
                                }else if (data.email == $("#user").val()){
                                    addCartVirtual(data.id_member);
                                    window.location.replace("page.php");
                                }
                            }else{
                                $.notify("Wrong credential...!", "warnning");
                                changeBtnLoginText();
                            }
                            
                        }
                    });
                }

                $("#login").on("submit",function(e){
                    e.preventDefault();
                    submitDataLogin();
                });

                $.ajax({
                    url:"controlers/testimonialControler.php",
                    type:"GET",
                    success:function(res){
                        let data = JSON.parse(res);
                        let html="";
                        for (let i=0; i < data["data"].length; i++){
                            html = '<div id="cmt'+i+'">' +
                                    '<strong id="member-name" style="color:dark-grey; font-size:0.7rem;">~'+ data["data"][i].firstname + ' ' + data["data"][i].lastname +'~</strong></br>'+
                                    '<small id="testi" style="font-style:italic; color:grey;">'+ data["data"][i].comment +'</small>'+
                                    '</div>';
                            $("#testi-area").append(html); 
                        }
                        testiFade();
                    }
                })
            });
            
            function testiFade(){
                let el = $('div[id^=cmt]'),
                    n = 0,
                    len = el.length;
                    el.slice(1).hide();
                        setInterval(function () {
                            el.eq(n).fadeOut(function () {
                                n = (n + 1) % len
                                el.eq(n).fadeIn();
                            })
                        }, 5000)
            }
            
            $('.carousel').carousel();
            
            function changeBtnLoginText(){
                if ($("#btn-submit").text() == "Login"){
                    $("#btn-submit").text("Processing...");
                }else{
                    $("#btn-submit").text("Login");
                }
            }
            
            $("#btn-submit").on("click",function(e){
                changeBtnLoginText();
            });
            
            $("#user").on("change",function(){
                this.value.toLowerCase();
            })
