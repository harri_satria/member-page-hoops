   let addItem = (id) => {
                $("#fItem"+id).on("submit",function(e){
                    e.preventDefault();
                    let pv = $(this).find("input[name='poin-value']").val();
                    let id_item = $(this).find("input[name='id-item']").val();
                    let exp_item = $(this).find("input[name='exp-item']").val();
                    let name_item = $(this).find("input[name='name-item']").val(); 
                    let type_item = $(this).find("input[name='type-item']").val();
                    let min_trans = $(this).find("input[name='min-trans']").val();

                    if (parseInt(pv) <= poinState){
                        addItemToCart(name_item,pv,id_item,exp_item,type_item,null,min_trans,null,null,id);
                        changeBtnSelectText(id);
                    }else{
                        $.notify(
                            "Ops..your points are not enough",
                            {
                                className:"error",
                                position:"bottom right"
                            }
                        );
                    }
                })
            }
                addCartCount();
                let tableTrans = $("#totTrans").DataTable({
                    processing:true,
                    serverSide:false,
                    ajax: "controlers/transDataControler.php",
                    order: [[ 0, "desc" ]],
                    columns: [
                        {"data":"transaction_date"},
                        {"data":"transaction_number"},
                        {"data":"transaction_value",render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp ' ),className:"text-right"},
                        {"data": "point_value"}
                    ],
                    columnDefs:[
                        {
                            targets:3,
                            className:"text-right",
                            render: function(data,type,row,meta){ 
                                return data + '&nbsp;<i class="fa fa-product-hunt" style="color:grey;"></i>'
                            }
                        }
                    ]
                });
                
                let tableRedeem = $("#totRedeem").DataTable({
                    processing:true,
                    serverSide:false,
                    ajax: "controlers/redeemDataControler.php",
                    order: [[ 0, "desc" ]],
                    language:{"emptyTable":"No data redeem yet"},
                    columns: [
                        {"data":"date_add"},
                        {"data":"id_order"},
                        {"data":"total_poin",className:"text-right"},
                        {
                            "data":null,
                            "className":"text-right",
                        }
                    ],
                    columnDefs:[
                        {
                            targets:2,
                            className:"text-right",
                            render: function(data,type,row,meta){ 
                                return data + '&nbsp;<i class="fa fa-product-hunt" style="color:grey;"></i>'
                            }
                        },
                        {
                            targets:3,
                            searchable: false,
                            orderable: false,
                            targets:-1,
                            render:function(data,type,row,meta){
                                return '<button type="button" class="btn btn-primary btn-sm" name="btn-detail-redeem" data-toggle="modal" data-target="#redeemDetail"><i class="fa fa-info-circle"></i>&nbsp;Info</button>';
                        
                            }
                        }
                    ]
                });

                let poinState;
                
                $.ajax({
                    url:"controlers/profileDataControler.php",
                    type:"POST",
                    success:function(res){
                        let data = JSON.parse(res); 
                        $("#idmember").append(data.idmember);    
                        $("#regdate").append(data.create_date);
                        $("#identity").append(data.identity_number);
                        $("#name").append(data.name.toLowerCase().replace(/\b(\w)/g, x => x.toUpperCase()));
                        $("#phone").append(data.phone);
                        $("#email").append(data.email.toLowerCase());
                        $("#city").append(data.city.toLowerCase().replace(/\b(\w)/g, x => x.toUpperCase()));
                        $("#address").append(data.address.toLowerCase().replace(/\b(\w)/g, x => x.toUpperCase()));
                        poinState = data.poin; 
                        $("#poin-info").html('<i class="fa fa-product-hunt"></i>&nbsp;'+ poinState);
                    }
                });


                $.ajax({
                    url:"controlers/voucherControler.php",
                    type:"POST",
                    data:{

                    },
                    success:function(res){
                        let data = JSON.parse(res);
                        let minimumTrx; 
                        
                        for (var i=0; i < data.length; i++){  
                            if (data[i].card_value.length <= 2){  
                                if (data[i].min_trans > 0){
                                    minimumTrx = data[i].min_trans;
                                }
                                let html =  '<div id="redeem" class="col-xs-6 col-sm-4 col-md-4 col-lg-3" style="padding-top:15px">' +
                                            '<div class="card">' +
                                                '<img src="assets/img/vouchers/'+data[i].img+'" class="card-img-top">' +
                                                '<div class="card-body" style="text-overflow:ellipsis;">' +
                                                    '<div class="text-center" style="font-weight:bold; font-size:small;text-transform:uppercase;"><span>'+ data[i].card_name +'&nbsp;'+ data[i].card_value +'%</span></div>' + 
                                                    '<div class="text-center"><small>Expired : ' + data[i].card_end + '</small></div>' +
                                                    '<div class="text-center"><label>'+ data[i].poin_required.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +'&nbsp;<i class="fa fa-product-hunt" data-toggle="tooltip" data-placement="right" title="the number of points you must have"></i></label></div>' +
                                                    '<form id="fItem'+i+'">' +
                                                        '<input type="hidden" id="poin-value" name="poin-value" value="'+ data[i].poin_required +'"/>' +
                                                        '<input type="hidden" name="id-item" value="'+ data[i].id +'"/>' +
                                                        '<input type="hidden" name="exp-item" value="'+ data[i].card_end +'"/>' +
                                                        '<input type="hidden" name="name-item" value="'+ data[i].card_name +'&nbsp;'+ data[i].card_value +'%"/>' +
                                                        '<input type="hidden" name="type-item" value="'+ 1 +'"/>' +
                                                        '<input type="hidden" name="min-trans" id="min-trans'+ i +'">' +
                                                        '<div class="text-center"><button type="submit" onclick="addItem('+i+')" class="btn btn-success" name="btn-select" id="btn-select'+i+'">Select</div>' +
                                                    '</form>' +
                                                    '<div class="lb-info" data-toggle="tooltip" title="Have a minimum Transaction"><small id="info1"></small></div>' +                
                                                '</div>' +
                                            '</div>' +
                                            '</div>'
                                        ;
                                        $("#card").append(html);
                                        if (data[i].min_trans > 0){ 
                                            $("#info1").append('<i class="fa fa-info-circle">&nbsp;</i>'+minimumTrx.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")); 
                                            $("#min-trans"+i).val("Minimum transaction " + minimumTrx.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                                        }
                                                
                            }else{
                                if (data[i].min_trans > 0){ 
                                    minimumTrx = data[i].min_trans;
                                }       
                                let html =  '<div id="redeem" class="col-xs-6 col-sm-4 col-md-4 col-lg-3" style="padding-top:15px">' +
                                            '<div class="card">' + 
                                                '<img src="assets/img/vouchers/'+data[i].img+'" class="card-img-top">' +
                                                '<div class="card-body" style="text-overflow:ellipsis;">' +
                                                    '<div class="text-center" style="font-weight:bold; font-size:small;text-transform:uppercase;"><span>'+ data[i].card_name +'&nbsp;Rp '+ data[i].card_value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +'</span></div>' + 
                                                    '<div class="text-center"><small>Expired : ' + data[i].card_end + '</small></div>' +
                                                    '<div class="text-center"><label>'+ data[i].poin_required.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +'&nbsp;<i class="fa fa-product-hunt" data-toggle="tooltip" data-placement="right" title="the number of points you must have"></i></label></div>' +
                                                    '<form id="fItem'+i+'">' +
                                                        '<input type="hidden" id="poin-value" name="poin-value" value="'+ data[i].poin_required +'"/>' +
                                                        '<input type="hidden" name="id-item" value="'+ data[i].id +'"/>' +
                                                        '<input type="hidden" name="exp-item" value="'+ data[i].card_end +'"/>' +
                                                        '<input type="hidden" name="name-item" value="'+ data[i].card_name +'&nbsp;Rp '+ data[i].card_value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +'"/>' +
                                                        '<input type="hidden" name="type-item" value="'+ 1 +'"/>' +
                                                        '<input type="hidden" name="min-trans" id="min-trans'+ i +'">' +
                                                        '<div class="text-center"><button type="submit" onclick="addItem('+i+')" class="btn btn-success" name="btn-select" id="btn-select'+i+'">Select</div>' +
                                                    '</form>' +   
                                                    '<div class="lb-info" data-toggle="tooltip" title="Have a minimum Transaction"><small id="info2"></small></div>' +             
                                                '</div>' +
                                            '</div>' +
                                            '</div>'
                                        ;
                                        $("#card").append(html);
                                        if (data[i].min_trans > 0){ 
                                            $("#info2").append('<i class="fa fa-info-circle">&nbsp;</i>'+minimumTrx.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                                            $("#min-trans"+i).val("Minimum transaction " + minimumTrx.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                                        }
                            }        
                          
                        }                        
                    }
                });

                let couponName;
                let idItem;
                let expItem;
                let poin;
                /* DISABLE SEMENTARA UNTUK KUPON RAFFLE
                $.ajax({
                    url:"controlers/raffleCouponControler.php",
                    type:"GET",
                    success:function(res){
                        let data = JSON.parse(res); //console.log(data);
                        for (var i=0; i < data.length; i++){
                            couponName = data[i].raffle_name;
                            idItem = data[i].id;
                            expItem = data[i].end_date;
                            poin = data[i].poin_required;
                        let html =  '<div id="redeem" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' +
                                        '<div class="card">' +
                                            '<img src="assets/img/voucher.png" class="card-img-top">' +
                                            '<div class="card-body" style="text-overflow:ellipsis;">' +
                                                '<div class="text-center text-truncate" style="font-weight:bold; font-size:small;text-transform:uppercase;">'+ data[i].raffle_name +'</div>' +
                                                '<div class="text-center"><small>End at: '+ data[i].end_date +'</small></div>' +
                                                '<div class="text-center"><label>'+(poin/1000).toFixed(3)+'&nbsp;<i class="fa fa-product-hunt"></i></label></div>' +
                                                '<div class="text-center"><button type="button" class="btn btn-warning" name="btn-select-coupon" data-toggle="modal" data-target="#productDetail" poin="25000" productId="'+ data[i].raffle_product_id +'" expItem="'+ data[i].end_date +'" idItem="'+ data[i].id +'" itemName="'+ data[i].raffle_name +'" itemType="2">Select</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '</div>'
                                    ;
                                    
                        $("#card").append(html) ;
                        }           
                    }
                }); */

                function addItemToCart(val,poin,idItem,expItem,itemType,size,desc,idProductRaffle,idVoucher,btnId){
                    $.ajax({
                        url:"controlers/itemControler.php",
                        type:"POST",
                        data:{
                            "cart_type":"v",
                            "item_name":val,
                            "poin":poin,
                            "idItem":idItem,
                            "expItem":expItem,
                            "itemType":itemType,
                            "size":size,
                            "desc":desc,
                            "idProductRaffle":idProductRaffle,
                            "idVoucher" : idVoucher,
                        },
                        success:function(res){
                            $.notify(
                                "Success adding an item to cart...",
                                {
                                    className:"success",
                                    position:"bottom-right"
                                }
                            );
                            changeBtnSelectText(btnId);
                            let cartCount = $("#cart-count").text();
                            let latestCount = parseInt(cartCount) + 1;
                            $("#cart-count").text(latestCount);
                            $("#list-item").html("");
                            addCartCount();
                            if (itemType == 2){
                                $("#productDetail").modal("hide");
                            }
                        }
                    });
                }

                function changeBtnSelectText(id){
                    if ($("#btn-select"+id).text() == "Select"){
                        $("#btn-select"+id).text("Wait...");
                    }else{
                        $("#btn-select"+id).text("Select");
                    }
                }

                function addCartCount(){
                    $.ajax({
                        url:"controlers/cartDataControler.php",
                        type:"GET",
                        data:{"from":"2"},
                        success:function(res){
                            let data = JSON.parse(res); //console.log(data);
                            let totalPoin=0;
                            if (data.length < 1){
                                $("#btnCheckout").prop("disabled",true);
                            }else{
                                $("#btnCheckout").prop("disabled",false);
                            }
                            $("#cart-count").html(data.length); 
                            for (var i=0; i < data.length; i++){
                                $("#list-item").append('<tr><td style="width:15rem;">'+data[i].item+'</td><td style="text-align:right;">'+data[i].poin.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+'&nbsp;<i class="fa fa-product-hunt"></i></td></tr>');
                                totalPoin += parseInt(data[i].poin);
                            }
                            $("#total-poin").html("");
                            $("#total-poin").append(totalPoin.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+"&nbsp;<i class='fa fa-product-hunt'></i>");
                        }
                    });
                }

                function decreasePoint(){
                    //console.log()
                }

                function createVoucher(){
                    let item=[];

                    tableCart.data().each(function(d){
                        item.push(d);
                    });
                    //console.log(item);
                           Swal.fire(
                                'Congratulation!',
                                'Your points have been successfully exchanged, You will find the voucher on the transaction summary page on the website',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed){
                                    /*$.ajax({
                                        url:"controlers/deleteCartListControler.php",
                                        type:"POST",
                                        data:{"dataArray2":item,"delFunc":"2"},
                                        success:function(res){

                                        }
                                    });*/

                                    $.ajax({
                                        url:"controlers/createVoucherControler.php",
                                        type:"POST",
                                        data:{
                                            "dataArray":item,
                                            "sumCart": $("#sum").text()
                                        },
                                        success:function(res){ 
                                            $.ajax({
                                                url:"controlers/cartDataControler.php",
                                                type:"GET",
                                                data:{"from":"3"},
                                                success:function(res){
                                                    //console.log("count->"+res);
                                                    $("#cart-count").text(res);
                                                    tableRedeem.ajax.reload();
                                                    tableMyVoucher.ajax.reload();
                                                    $("#poin-info").html('<i class="fa fa-product-hunt"></i>&nbsp;'+ poinState);
                                                }
                                            });
                                        }
                                    });
                                    
                                    $('.list-group a[href="#myvoucher"]').tab('show');
                                }
                            }); 
                }

                function populateItem(idCart){
                    $("#redeemDetailTable").DataTable().clear().destroy();

                    let redeemDetail = $("#redeemDetailTable").DataTable({
                        processing:true,
                        serverSide:false,
                        searching:false,
                        paging:true,
                        info:false,
                        responsive:true, 
                        ajax:{
                            url:"controlers/redeemItemControler.php",
                            type:"POST",
                            data:{"idCartSelected":idCart}
                        },
                        columnDefs:[
                            {
                                "render":function(data,type,row){
                                    return data +'&nbsp;<i class="fa fa-product-hunt"></i>';
                                },
                                "targets":2 
                            }
                        ],
                        columns: [
                            {"data":null},
                            {"data":"item_name"},
                            {"data":"poin",className:"text-right"},
                            {"data":"description"},
                            {"data":"exp_item"}
                        ],
                        createdRow : function(row,data,index){
                            let d = new Date();

                            let month = d.getMonth()+1;
                            let day = d.getDate();
                            let hour = d.getHours();
                            let min = d.getMinutes();
                            let sec = d.getSeconds();

                            let output = d.getFullYear() + '-' +
                                ((''+month).length<2 ? '0' : '') + month + '-' +
                                ((''+day).length<2 ? '0' : '') + day + ' ' + hour + ':' + min + ':' + sec ;

                            if (data.exp_item < output){
                                $("td", row).eq(4).addClass("text-danger");
                            }else{
                                $("td", row).eq(4).addClass("text-info");
                            }
                        }
                    });

                    redeemDetail.on( 'order.dt search.dt', function () {
                        redeemDetail.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            cell.innerHTML = i+1;
                        } );
                    } ).draw();

                }

                function countChar(val) {
                    var len = val.value.length;
                    if (len >= 150) {
                        val.value = val.value.substring(0, 150);
                    } else {
                        $('#charNum').text(150 - len + " / 150") ;
                    }
                };

                $("#comment").keyup(function(){
                    countChar(this);
                });

                $("#testimonial form").on("submit",function(e){
                    e.preventDefault();
                    $.ajax({
                        url:"controlers/testimonialFormControler.php",
                        type:"POST",
                        data:($(this).serialize()),
                        success:function(res){
                            if (res == 200){
                                $.notify(
                                    "Thank you for writing your opinion...",
                                    {
                                        className:"success",
                                        position:"bottom-right"
                                    }
                                );
                            }else{
                                $.notify(
                                    "You have written your opinion before...",
                                    {   
                                        className: "error",
                                        position:"bottom right"
                                    }
                                );
                            }
                        }
                    });
                });
                let sumCart;
                let tableCart = $("#listCart").DataTable({
                    processing:true,
                    serverSide:false,
                    searching:false,
                    paging:false,
                    info:false,
                    responsive:true, 
                    language:{
                        emptyTable:"No item in this cart"
                    },
                    ajax:{
                        url:"controlers/cartDataControler.php",
                        type:"GET",
                        data:{"from":"1"},
                    },
                    columnDefs: [
                        {
                            "searchable": false,
                            "orderable": false,
                            "targets": 0
                        },
                        {
                            "searchable": false,
                            "orderable": false,
                            "targets":-1,
                            "render":function(data,type,row,meta){
                                return '<button type="button" class="btn btn-danger btn-sm" name="btn-remove">&times;</button>';
                        
                            },
                            "className":"text-right"
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 1
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 2
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 4
                        },
                        {
                            "targets": 6,
                            "render": function(data,type,row,meta){ 
                                return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + '&nbsp;<i class="fa fa-product-hunt"></i>'
                            }
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 7
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 10
                        },
                        {
                            "searchable": false,
                            "visible": false,
                            "targets": 8
                        }
                    ],
                    columns:[
                        {"data":null},
                        {"data":"id_cart"},
                        {"data":"id_item"},
                        {"data":"exp_item"},
                        {"data":"item_type"},
                        {"data":"item_name"},
                        {"data":"poin",className:"text-right"},
                        {"data":"id"},
                        {"data":"size"},
                        {"data":"description"},
                        {"data":"id_product"},
                        {"data":null}
                    ],
                    drawCallback:function(){
                        sumCart = $("#listCart").DataTable().column(6).data().sum();
                        $("#sum").text(sumCart.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                    },
                    createdRow : function(row,data,index){ 
                        $("td", row).eq(4).addClass("text-info");
                    }
                
                });

                tableCart.on( 'order.dt search.dt', function () {
                    tableCart.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

               /* $("body").on("click","button[name='btn-select']", function(e){ 
                    if (parseInt($(this).attr("poin")) <= poinState){
                        //addItemToCart($(this).val(),$(this).attr("poin"),$(this).attr("idItem"),$(this).attr("expItem"),$(this).attr("itemType"));
                    }else{
                        $.notify(
                            "Ops..your points are not enough",
                            {
                                className:"error",
                                position:"bottom right"
                            }
                        );
                    }
                }); */
            
            
                $("body").on("click","button[name='btn-select-coupon']",function(){
                    //addItemToCart($(this).attr("itemName"),$(this).attr("poin"),$(this).attr("idItem"),$(this).attr("expItem"),$(this).attr("itemType")) // DISNI HARUS ADA INFORMASI PARAMETER item_type
                    if (parseInt($(this).attr("poin")) <= poinState){
                        $.ajax({
                            url:"controlers/raffleProductDetailControler.php",
                            type:"GET",
                            data:{"idRaffle":$(this).attr("idItem")},
                            success:function(res){
                                let data = JSON.parse(res); 
                                $("#size-option").html("");

                                $("#item-name").val(couponName);
                                $("#poin").val($('button[name="btn-select-coupon"]').attr("poin"));
                                $("#item-id").val(idItem);
                                $("#exp-item").val(expItem);
                                $("#item-type").val("2");

                                for (var i=0; i < data.length; i++){
                                    $("#raffle-id").val(data[i].id);
                                    $("#product-id").val(data[i].raffle_product_id);
                                    let o = new Option(data[i].size,data[i].size_id);
                                    $("#size-option").append(o);    
                                }
                            }
                        });
                    }else{
                        $.notify(
                            "Ops..your points are not enough",
                            {
                                className:"error",
                                position:"bottom right"
                            }
                        );
                    }
                });

                $("#btn-size").on("click",function(){
                    addItemToCart(couponName,poin,idItem,expItem,2,$("#size-option option:selected").val(),$("input[name='opt-shipping']:checked").val(),$("#product-id").val());
                });

                $("#listCart tbody").on("click","button[name='btn-remove']",function(){
                        let tr = $(this).closest("tr");
                        let dataSel = tableCart.row($(this).parents("tr")).data();
                        $.ajax({
                            url:"controlers/deleteCartListControler.php",
                            type:"POST",
                            data:{
                                "idRowItem":dataSel.id, 
                                "delFunc":"1"
                            },
                            success:function(res){
                                tr.fadeOut(800,function(){
                                    tableCart.row(tr).remove().draw(); // HANYA REMOVE YG DIPILIH
                                    //tableCart.ajax.reload(); PERFORMANCE ISSUE, SEMUA DATA DI LOAD
                                    let cartCount = $("#cart-count").text();
                                    $("#cart-count").text(cartCount-1);

                                    let sumCart = tableCart.column(6).data().sum();
                                    $("#sum").text(sumCart);
                                    $("#list-item").html("");
                                    addCartCount();
                                });
                            }
                        });
                });
                
                $("#totRedeem tbody").on("click","button[name='btn-detail-redeem']",function(){
                    let data = tableRedeem.row($(this).parents("tr")).data();
                    populateItem(data.id_cart);
                    $("#date-info").empty();
                    $("#date-info").append("Redeem date : " + data.date_add);
                });

                $("#btn-confirm-checkout").on("click",function(){
                    if (parseInt(poinState) >= parseInt(sumCart)){ 
                        Swal.fire({
                            icon:'question',
                            title:'Are you sure!',
                            text:'Points will be reduced according to the voucher you chose after you press the Redeem button',
                            showDenyButton: true,
                            confirmButtonText: 'Redeem',
                        }).then((result) => {
                            if (result.isConfirmed){
                                createVoucher();
                                tableMyVoucher.ajax.reload();
                                //tableCart.ajax.reload();
                            }
                        });
                    }else{
                        Swal.fire({
                            icon:'error',
                            title:'Not enough points...!',
                            text: 'Your points are not enough to redeem with these items',
                            confirmButtonText: 'Ok i understand',
                        })
                    }
                });    

                let hash = window.location.hash;

                $("#btnCheckout").on("click",function(e){
                    id = $(e.target).attr("href").substr(1);
                    window.location.hash = id;
                    window.location.reload(); //PERFORMANCE ISSUE, BUT HOW ELSE.....!
                });

                $(".list-group > a").on("shown.bs.tab",function(e){
                    let id = $(e.target).attr("href").substr(1);
                    window.location.hash = id;
                });
                
                $('.list-group a[href="' + hash + '"]').tab('show');
                $('.dropdown-item-nohover button[href="' + hash + '"]').tab('show');

                $("#menu-toggle").click(function (e) {
                    e.preventDefault();
                    $("#wrapper").toggleClass("toggled");
                });

                $('[data-toggle="tooltip"]').tooltip();

                $('#myModal').on('shown.bs.modal', function () {
                    $('#myInput').trigger('focus')
                });

                //get opini
                $.ajax({
                    url:"controlers/opiniControler.php",
                    type:"GET",
                    data:{
                        "idMember":"<?php echo $_SESSION['memberId'] ?>"
                    },
                    success:function(res){
                        if (res !== ""){
                            let data = JSON.parse(res);
                            $("#opini").append(data.opini);
                            $("#dateCreate").append(data.dateCreate);
                        }else{
                            $("#opini").append("You haven't written your opinion about hoops indonesia membership");
                            $("#opini").css("font-style","unset");
                            $("#opini").css("color","lightgrey");
                        }
                    }
                });

                //LIST MY VOUCHER
                let tableMyVoucher = $("#table-myvoucher").DataTable({
                    processing:true,
                    serverSide:false,
                    responsive:true,
                    ajax:"controlers/myVoucherControler.php",
                    columns:[
                        {"data":null},
                        {"data":"card_name"},
                        {"data":"card_value"},
                        {"data":"card_minimum_transaction",render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp ' ),className:"text-right"},
                        {"data":"poin_required"},
                        {"data":"createdAt"},
                        {"data":"card_start"},
                        {"data":"card_end"}
                    ],
                    columnDefs:[
                        {
                            targets:2,
                            render: function(data,type,row,meta){ 
                                if (data.length <= 2){
                                    return data + '%';
                                }else{
                                    return "Rp " + data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                                }
                            }
                        },
                        {
                            targets:4,
                            className:"text-right",
                            render:function(data,type,row,meta){
                                return data + '&nbsp;<i class="fa fa-product-hunt" style="color:grey;"></i>';
                            }
                        },
                        {
                            targets:7,
                            render:function(data,type,row){
                                let d = new Date();

                                let month = d.getMonth()+1;
                                let day = d.getDate();
                                let hour = d.getHours();
                                let min = d.getMinutes();
                                let sec = d.getSeconds();

                                let output = d.getFullYear() + '-' +
                                    ((''+month).length<2 ? '0' : '') + month + '-' +
                                    ((''+day).length<2 ? '0' : '') + day + ' ' + hour + ':' + min + ':' + sec ;

                                if (data < output){
                                    $("td", row).eq(7).addClass("text-danger"); return data + " (Expired)";
                                }else{
                                    return data;
                                }
                            }
                        }
                        
                    ],
                    createdRow : function(row,data,index){
                            let d = new Date();

                            let month = d.getMonth()+1;
                            let day = d.getDate();
                            let hour = d.getHours();
                            let min = d.getMinutes();
                            let sec = d.getSeconds();

                            let output = d.getFullYear() + '-' +
                                ((''+month).length<2 ? '0' : '') + month + '-' +
                                ((''+day).length<2 ? '0' : '') + day + ' ' + hour + ':' + min + ':' + sec ;

                            if (data.card_end < output){
                                $("td", row).eq(7).addClass("text-danger"); 
                            }
                            
                            if (data.reedem === "1"){
                                $("td", row).eq(7).text('Used on ' + data.updatedAt).addClass("text-primary");
                            }
                        }
                });
                tableMyVoucher.on( 'order.dt search.dt', function () {
                        tableMyVoucher.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            cell.innerHTML = i+1;
                        } );
                 } ).draw();
