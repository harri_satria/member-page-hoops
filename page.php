
<?php
    require_once "config.php";
    session_start(); 

    $sql = "SELECT * FROM hoops_members WHERE idcustomer = ?";
    $res = $conn->prepare($sql);
    $res->execute([$_SESSION["userId"]]);
    $data = $res->fetchAll();
    foreach ($data as $row){
        $name = $row["firstname"];
        $poin = $row["pointstate"];
    } 
?>
<?php
    if (!isset($_SESSION["userId"])){
        header("location:index.html");
    }else if (isset($_SESSION["LASTLOGIN"]) && (time() - $_SESSION["LASTLOGIN"] > 900)){
        session_unset();
        header("location:index.php");
    }else{    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title></title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading"><img src="assets/img/logoMember.png" class="img-fluid"></div>
                <div class="list-group list-group-flush">
                    <a class="list-group-item list-group-item-action bg-light" id="tab-dashboard" data-toggle="pill" role="tab" aria-controls="dashboard" href="#dashboard"><i class="fa fa-home"></i>&nbsp;&nbsp;<span>Dashboard</span></a>
                    <a class="list-group-item list-group-item-action bg-light" id="tab-profile" data-toggle="pill" role="tab" aria-controls="profile" href="#profile"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;<span>Profile</span></a>
                    <a class="list-group-item list-group-item-action bg-light" id="tab-redeem" data-toggle="pill" role="tab" aria-controls="shortcut" href="#redeem"><i class="fa fa-certificate"></i>&nbsp;&nbsp;<span>Redeem Poin</span></a>
                    <a class="list-group-item list-group-item-action bg-light" id="tab-myvoucher" data-toggle="pill" role="tab" aria-controls="myvoucher" href="#myvoucher"><i class="fa fa-gift"></i>&nbsp;&nbsp;<span>My Voucher</span></a>  
                    <a class="list-group-item list-group-item-action bg-light" id="tab-testimonial" data-toggle="pill" role="tab" aria-controls="testimonial" href="#testimonial"><i class="fa fa-comment"></i>&nbsp;&nbsp;<span>Testimonial</span></a>
                    <a class="list-group-item list-group-item-action bg-light" id="tab-contact" data-toggle="pill" role="tab" aria-controls="contactUs" href="#contactUs"><i class="fa fa-phone"></i>&nbsp;&nbsp;<span>Contact Us</span></a>
                    <a class="list-group-item list-group-item-action bg-light" id="tab-term" data-toggle="pill" role="tab" aria-controls="userTerm" href="#userTerm"><i class="fa fa-gavel"></i>&nbsp;&nbsp;<span>Terms & Condition</span></a>

                </div>
            </div>
            <!-- Page Content-->
            <div id="page-content-wrapper">
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn" id="menu-toggle"><i class="fa fa-exchange"></i></span></button>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <p class="nav-link dropdown-toggle" id="dropdown-cart" href="#!" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-shopping-cart"></i>&nbsp;
                                    <span id="cart-count">0</span>
                                </p>
                                <div class="dropdown-menu dropdown-menu-right" style="width: max-content;" aria-labelledby="dropdown-cart">
                                    <div class="dropdown-item-nohover">
                                        <table id="list-item" style="font-size:0.7rem;">
                                            
                                        </table>
                                    </div>
                                    <div class="dropdown-item-nohover">
                                        <hr/>
                                        <span>Total</span><span style="float: right;" id="total-poin"></span>
                                    </div>
                                    <div class="dropdown-item-nohover" style="margin-top:10px;">
                                        <button class="btn btn-success btn-sm" data-toggle="pill" role="tab" aria-controls="cart" href="#cart" id="btnCheckout">Checkout</button>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <p class="nav-link" id="poin-info">
                                </p>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#!" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform:capitalize;"><i class="fa fa-user">&nbsp;</i><?php echo $name; ?></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="controlers/logoutControler.php">Logout</a>
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>    
                <div class="tab-content">
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-term" id="userTerm">
                        <h1 class="mt-4">Terms and condition</h1>
                        <ul>
                            <li>
                                HOOPS INDONESIA member program is a customer loyalty program offered by 
                                PT JAVAFOOTWEAR SPORTINDO to its customers .
                            </li>
                            <li>
                                The membership program is free. Membership registration can be done at our offline or online store after a minimum purchase of IDR 1.500.000.
                            </li>
                            <li>
                                These Terms & Conditions constitute an agreement between Members and HOOPS INDONESIA with respect to the membership program. By participating in the membership program, member agrees to these Terms & Conditions.
                            </li>
                            <li>
                                HOOPS INDONESIA reserves the right to change the Terms & Conditions at any time for any reason without prior written notice. Member is responsible for keeping track of any update and changes on the Terms & Conditions.
                            </li>
                            <li>
                                HOOPS INDONESIA reserves the right to terminate the membership program at any time for any reason. 
                            </li>
                            <li>Membership is only available to individuals. For corporations, charities, partnerships, or any other entity membership, please contact +6281 122 0817 or email us at sales@hoopsindonesia.co.id
                            </li>
                            <li>
                                Membership program is open to Indonesian residents at least 17 years of age.
                            </li>
                            <li>
                                HOOPS INDONESIA reserves the right to suspend or terminate the Membership if: 
                                <ul>
                                    <li>Member is in breach of the HOOPS INDONESIA Terms & Conditions.</li>
                                    <li>Member has not been active within 12 months.</li>
                                    <li>Member has given incomplete, inaccurate, false, or fictitious personal information</li>
                                    <li>Member commits fraudulent or unauthorized use of HOOPS INDONESIA membership.</li>
                                </ul>
                            <li>
                                Member should promptly notify HOOPS INDONESIA of any changes of personal information such as name, address, telephone numbers, and email address by calling HOOPS INDONESIA contact at +6281 122 0187 or email us at sales@hoopsindonesia.co.id
                            </li>        
                            <li>
                                A member may terminate his/her membership at any time by contacting HOOPS INDONESIA at +6281 122 0187 or email us at sales@hoopsindonesia.co.id
                            </li>
                            <li>
                                By participating in the membership program, member agrees to allow HOOPS INDONESIA to communicate via mail, email, phone, text messages, and notification via mobile App or website. HOOPS INDONESIA may use these channels to communicate member’s account status, notify member when they are eligible for a reward, communicate program changes, offer special member promotions, information, and offerings that HOOPS INDONESIA believes may be of interest to Member. HOOPS INDONESIA will protect Member’s data in accordance with the prevailing laws in Indonesia.
                            </li>
                            <li>
                                A Member can have multiple cards from HOOPS INDONESIA, however all cards must be registered to 1 member ID.
                            </li>
                            <li>
                                The loss of Physical Card must be reported to HOOPS INDONESIA immediately. In case the physical pard is lost or physically damaged, HOOPS INDONESIA may, upon the member’s request, issue a digital card after the loss or the damage has been reported. 
                            </li>
                            <li>
                                Unauthorized reproduction of the membership card may lead to legal prosecution and forfeiture of membership and all points earned.
                            </li>
                        </ul>
                        <small>Date of revision : September 2021</small>
                    </div>
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-profile" id="profile">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Member Profile</h4>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm table-striped" id="table-profile">
                                            <tr>
                                                <td>Member id</td>
                                                <td class="row-profile" id="idmember">: </td>
                                            </tr>
                                            <tr>
                                                <td>Registration date</td>
                                                <td id="regdate">: </td>
                                            </tr>
                                            <tr>
                                                <td>Personal indentity number</td>
                                                <td id="identity">: </td>
                                            </tr>
                                            <tr>
                                                <td>Member name</td>
                                                <td id="name">: </td>
                                            </tr>
                                            <tr>
                                                <td>Contact number</td>
                                                <td id="phone">: </td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td id="email">: </td>
                                            </tr>
                                            <tr>
                                                <td>City</td>
                                                <td id="city">: </td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td id="address">: </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-contact" id="contactUs">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Head Office</h4>
                                    </div>
                                    <div class="card-body">
                                        <p>Jl. Surapati No.61, Coblong, Bandung, Jawa Barat 40133</p>
                                        <p>+62 811 220 187</p>
                                        <p>Operational hour Monday to Friday, 08:00 - 17:00 WIB</br>
                                        (except national holidays)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane active container-fluid" role="tabpanel" aria-labelledby="tab-dashboard" id="dashboard">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Important message!</strong> hello our loyal customers, the data below is your transaction data, and the number of points you get from that transaction.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card" style="margin-bottom: 25px;overflow-x:scroll;">
                                    <div class="card-header bg-dark text-white" ><h5>Transaction</h5></div>
                                    <div class="card-body">
                                    <table class="table table-striped dt-responsive" id="totTrans" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <td>Date</td>
                                                <td>Code Trx.</td>
                                                <td>Amount</td>
                                                <td>Poin Get</td>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card" style="margin-bottom: 25px;overflow-x:scroll;">
                                    <div class="card-header bg-dark text-white"><h5>Redeemed Poin</h5></div>
                                    <div class="card-body">
                                        <table class="table table-striped table-sm dt-responsive" id="totRedeem" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <td>Date</td>
                                                    <td>Redeem Id</td>
                                                    <td>Poin Used</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-myvoucher" id="myvoucher">
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <strong>For your info!</strong> You can use this voucher when you make a transaction on the website, in the summary cart you can select the voucher you have.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><h5>Voucher from redeemed poin</h5></div>
                                    <div class="card-body">
                                        <table id="table-myvoucher" class="table table-striped" style="width: 100%;">
                                            <thead class="bg-dark text-white">
                                                <tr>
                                                    <td>#</td>
                                                    <td>Voucher Name</td>
                                                    <td>Discount</td>
                                                    <td>Minimum Trx</td>
                                                    <td>Poin Req</td>
                                                    <td>Redeem Date</td>
                                                    <td>Effective Date</td>
                                                    <td>Expire Date</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-redeem" id="redeem">
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <strong>For your info!</strong> Point redemption items and nominal will change at any time without notification.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <span>
                        <h4 class="mt-2">
                            Gift List</h4>
                            List of gifts that you can exchange for the points you have
                        </span>
                        <hr />
                        <div class="row clearfix" id="card">
                        </div>
                    </div>
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-testimonial" id="testimonial">
                        <h1 class="mt-4">Testimonial</h1>
                        <p>Hello our loyal customers, now you can fill in your opinion here regarding hoops Indonesia membership, we hope you fill in a good comment.</p>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header"><h5>Leave your opinion here</h5></div>
                                    <div class="card-body">
                                        <form id="testimonial">
                                            <div class="form-group">
                                                <label>Maximum 150 characters</label>
                                                <textarea maxlength="150" name="comment" id="comment" class="form-control"></textarea>
                                                <small id="charNum" class="form-text text-right">150 / 150</small>
                                            </div>
                                            <div class="from-group">
                                                <button type="submit" name="btn-testimonial" class="btn btn-primary">Submit</button>
                                            </div>    
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header"><h5>The opinion you have written</h5></div>
                                    <div class="card-body">
                                        <div class="">
                                            <div id="opini" style="font-style: italic;"></div>
                                            <hr>
                                            <small id="dateCreate"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PRODUK RAFFLE DETAIL MODAL-->
                    <div class="modal fade" id="productDetail" tabindex="-1" aria-labelledby="productDetail" aria-hidden="true">
                        <div class="modal-dialog modal-md modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title">Raffle Product Detail</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body col-md-6 col-sm-12">
                                <input type="hidden" id="raffle-id">
                                <input type="hidden" id="product-id">
                                <input type="hidden" id="item-name">
                                <input type="hidden" id="poin">
                                <input type="hidden" id="item-id">
                                <input type="hidden" id="exp-item">
                                <input type="hidden" id="item-type">

                                <div class="form-group">
                                    <label>Select Size</label>
                                    <select id="size-option" class="form-control"></select>
                                </div>
                                <label>Shipping Option</label>
                                <div class="form-check">
                                    <input type="radio" name="opt-shipping" id="delivery" value="delivery" class="form-check-input" checked>
                                    <label for="delivery" class="form-check-label">Delivery</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" name="opt-shipping" id="take" value="take" class="form-check-input"> 
                                    <label for="take">Take Self</label>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" id="btn-size">Add to Cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane fade container-fluid" role="tabpanel" aria-labelledby="tab-cart" id="cart">
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <strong>Important message!</strong> hello our loyal customers, after you successfully redeem, you will find the voucher on the transaction summary page on the website.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="card" style="margin-bottom:1rem;">
                                    <div class="card-header">
                                        <h4>Item in cart</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row clearfix">
                                        <div class="col-md-12">
                                        <div style="overflow-x:scroll">
                                        <table class="table responsive" id="listCart" style="width: 100%;">
                                            <thead style="background-color:#f7f7f7;">
                                                <tr>
                                                    <td>#</td>
                                                    <td>idCart</td>
                                                    <td>idItem</td>
                                                    <td>Expired at</td>
                                                    <td>itemType</td>
                                                    <td>Item</td>
                                                    <td>Poin required</td>
                                                    <td>id</td>
                                                    <td>size</td>
                                                    <td>Desc</td>
                                                    <td>idProduct</td>
                                                    <td style="width: 10%;">Action</td>  
                                                </tr>
                                            </thead>
                                            <tbody id="cartItemContainer">
                                            </tbody>
                                        </table> 
                                        </div>
                                        </div>
                                        </div>
                                        <br>
                                        <div class="row clearfix">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body" style="text-align: right; font-weight: 700;">
                                                        <span>Cart total :&nbsp;</span>
                                                        <span id="sum">0</span>
                                                        <span><i class="fa fa-product-hunt"></i></span>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="button" class="btn btn-primary" id="btn-confirm-checkout" style="float:right;">Confirm & Redeem</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- Modal Detail Redeem-->
            <div class="modal fade" id="redeemDetail" tabindex="-1" aria-labelledby="redeemDetail" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="titleRedemDeatil">Redeem Items</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="col-md-12 text-right">
                        <div id="date-info"></div>
                    </div>
                    <div class="modal-body">
                        <table id="redeemDetailTable" class="table" style="width: 100%;">
                            <thead>
                                <tr style="background-color: whitesmoke;">
                                    <td>#</td>
                                    <td>Item Name</td>
                                    <td>Poin Required</td>
                                    <td>Description</td>
                                    <td>Expired at</td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.24/api/sum().js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="js/notify.js"></script>
        <script src="js/page.js"></script>

        
        <!-- Histats.com  START  (aync)-->
        <script type="text/javascript">var _Hasync= _Hasync|| [];
        _Hasync.push(['Histats.start', '1,4591535,4,0,0,0,00010000']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();</script>
        <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4591535&101" alt="hitcounter" border="0"></a></noscript>
        <!-- Histats.com  END  -->
    </body>
</html>
<?php
    }
?>